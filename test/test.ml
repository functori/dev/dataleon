open Dataleon

(* let (let>) = Lwt.bind *)
let (let>?) p f = Lwt.bind p (function Error e -> Lwt.return_error e | Ok x -> f x)

let print_error (code, msg) =
  Format.eprintf "Error (%d)%s@." code @@ Option.fold ~none:"" ~some:(fun s -> ": "^s) msg

type req = {
  first_name: string option;
  last_name: string option;
  company_name: string option;
  siren: string option;
  callback: string; [@anon 0]
} [@@deriving arg]

type command =
  | Req of req
  | Task of string
  | Delete of string
  | List
  | Decode of string
[@@deriving arg]

type config = {
  workspace: string option;
  api_key: string option;
  command: command;
} [@@deriving arg {exe="test.exe"}]

let wrap enc req =
  let>? req = req in
  Format.printf "Ok\n%s@." @@ EzEncoding.construct ~compact:false enc req;
  Lwt.return_ok ()

let () =
  let c = parse_config () in
  Option.iter (fun w -> workspace := w) c.workspace;
  Option.iter (fun k -> api_key := k) c.api_key;
  Lwt_main.run @@
  Lwt.map (Result.iter_error print_error) @@
  match c.command with
  | Req { first_name; last_name; company_name; siren; callback } ->
    let kind = match first_name, last_name, company_name, siren with
      | Some first_name, Some last_name, _, _ ->
        Types.(KYC { first_name; last_name; phone_number=None; country=None; gender=None })
      | _, _, Some company_name, Some siren ->
        Types.(KYB { company_name; siren; country=None; client_id=None; phone_support=None; email=None })
      | _ -> failwith "missing arguments" in
    wrap Types.task_enc (request ~kind callback)
  | Task id -> wrap Types.task_enc (get id)
  | Delete id -> wrap Types.task_enc (delete id)
  | List -> wrap  (Json_encoding.list Types.task_enc) (list ())
  | Decode file ->
    let ic = open_in file in
    let s = really_input_string ic (in_channel_length ic) in
    wrap Types.task_enc (Lwt.return_ok (EzEncoding.destruct Types.task_enc s))
