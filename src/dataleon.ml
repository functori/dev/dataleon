module Types = Dataleon_types
open Types

let api_key = ref ""
let endpoint = ref "https://inference.eu-west-1.dataleon.ai"
let workspace = ref ""

let lfold f o = Option.fold ~none:[] ~some:(fun x -> [ f x ]) o

let request ?workspace:wk ?(qr_code=true) ?tags ?notification ~kind callback =
  let headers = [
    "content-type", "multipart/form-data";
    "api_key", !api_key
  ] in
  let cb_notification = lfold (fun url -> "callback_url_notification", `content url, None) notification in
  let qr_code = if qr_code then [ "qr_code", `content "yes", None ] else [] in
  let workspace = Option.value ~default:!workspace wk in
  let tags = match tags with
    | None | Some [] -> []
    | Some l ->
      let s = EzEncoding.construct (Json_encoding.list tag_enc) @@
        List.map (fun (tkey, tvalue) -> {tkey; tvalue}) l in
      [ "tags", `content s, None ] in
  let kind_args = match kind with
    | KYC r -> [
        "first_name", `content r.first_name, None;
        "last_name", `content r.last_name, None; ] @
        (lfold (fun s -> "phone_number", `content s, None) r.phone_number) @
        (lfold (fun s -> "country", `content s, None) r.country) @
        (lfold (fun s -> "gender", `content s, None) r.gender)
    | KYB r -> [
        "company_name", `content r.company_name, None;
        "company_registration_id", `content r.siren, None; ] @
        (lfold (fun s -> "company_country", `content s, None) r.country) @
        (lfold (fun s -> "client_id", `content s, None) r.client_id) @
        (lfold (fun s -> "phone_support", `content s, None) r.phone_support) @
        (lfold (fun s -> "email", `content s, None) r.email) in
  let url = Format.sprintf "%s/%s" !endpoint (match kind with KYC _ -> "identity-request" | KYB _ -> "company-request") in
  Fun.flip Lwt.map (EzCurl_lwt.make ~meth:"POST" ~headers ~url @@ ([
    "workspace_id", `content workspace, None;
    "callback_url", `content callback, None;
    "path", `content "/kyc-verification", None;
  ] @ kind_args @ qr_code @ cb_notification @ tags)) @@ function
  | Error e -> Error e
  | Ok s ->
    let t = EzEncoding.destruct task_enc s in
    match t.code, t.link with
    | None, None -> Error (0, Some "No code or link found")
    | _ -> Ok t

let wrap_result r =
  Result.map_error (function
    | EzReq_lwt_S.UnknownError {code; msg} -> code, msg
    | EzReq_lwt_S.KnownError {code; error} -> code, Some (Printexc.to_string error)) r

let task_arg = EzAPI.Arg.string "task_id"

let%get get_task = { path="/task/{task_arg}"; output=task_enc }

let get id =
  let headers = [ "api_key", !api_key ] in
  Lwt.map wrap_result (EzReq_lwt.get1 ~headers (EzAPI.BASE !endpoint) get_task id)

let%delete delete_task = { path="/task/{task_arg}"; output=task_enc }

let delete id =
  let headers = [ "api_key", !api_key ] in
  Lwt.map wrap_result (EzReq_lwt.get1 ~headers (EzAPI.BASE !endpoint) delete_task id)

let status_param = EzAPI.Param.string "status"
let limit_param = EzAPI.Param.int "limit"
let offset_param = EzAPI.Param.int "offset"
let start_param = EzAPI.Param.string "start_date"
let end_param = EzAPI.Param.string "end_date"

let filter_params = [
  status_param;
  limit_param;
  offset_param;
  start_param;
  end_param;
]

let%get list_task = { path="/tasks"; output=Json_encoding.list task_enc; params=filter_params }

let list ?limit ?offset ?status ?start ?end_ () =
  let headers = [ "api_key", !api_key ] in
  let params =
    (lfold (fun status -> status_param, EzAPI.S status) status) @
    (lfold (fun limit -> limit_param, EzAPI.I limit) limit) @
    (lfold (fun offset -> offset_param, EzAPI.I offset) offset) @
    (lfold (fun start -> start_param, EzAPI.S start) start) @
    (lfold (fun end_ -> end_param, EzAPI.S end_) end_) in
  Lwt.map wrap_result (EzReq_lwt.get0 ~headers ~params (EzAPI.BASE !endpoint) list_task)
