type tag = {
  tkey: string;
  tvalue: Json_repr.ezjsonm;
} [@@deriving encoding]

type common = {
  action: string option;
  document_id: Json_repr.ezjsonm option;
  document_name: Json_repr.ezjsonm option;
  document_number: int option;
  id: string;
  name: string;
  page: int option;
  step: string option;
} [@@deriving encoding {ignore}]

type entity = {
  e_common: common; [@merge]
  bounding_box: Json_repr.ezjsonm;
  color: string option;
  confidence: float;
  text: string option;
  value: Json_repr.ezjsonm option;
} [@@deriving encoding]

type validation = {
  v_common: common; [@merge]
  code: string option;
  message: string;
  nodes: string list;
  tags: string list; [@dft []]
  type_: string; [@key "type"]
  type_validation: string;
  validate: bool;
  weight: int;
} [@@deriving encoding]

type class_ = {
  c_common: common; [@merge]
  c_confidence: float;
  c_color: string option;
} [@@deriving encoding {remove_prefix=2}]

type form = {
  f_document_type: string option;
  f_document_country: string option;
} [@@deriving encoding {remove_prefix=2}]

type metadata = {
  m_task_id: string option;
  m_status: string option;
} [@@deriving encoding {ignore}]

type response = {
  metadata: metadata;
  entities: entity list;
  validations: validation list;
  class_: class_ list; [@key "class"]
  alerts: Json_repr.ezjsonm list;
  form: form;
  documents: Json_repr.ezjsonm;
} [@@deriving encoding {ignore}]

type task = {
  task_id: string;
  status: string;
  code: string option;
  link: string option;
  response: response option;
} [@@deriving encoding {ignore}]

type tasks = {
  total: int;
  results: task list;
} [@@deriving encoding {ignore}]

type kyc_request_input = {
  first_name: string;
  last_name: string;
  phone_number: string option;
  country: string option;
  gender: string option;
}

type kyb_request_input = {
  company_name: string;
  siren: string;
  country: string option;
  client_id: string option;
  phone_support: string option;
  email: string option;
}

type request_kind =
  | KYC of kyc_request_input
  | KYB of kyb_request_input
